# Frontend
Dieses Repository enthält den Code des Frontends des Educational-Software-Prototyps zum Einstieg in das Thema der Kantenvorhersage in sozialen Netzwerken. 
Der Prototyp wurde im Rahmen einer Bachelorarbeit an der TH Nürnberg Georg Simon Ohm entwickelt.

## Für das Frontend benötigt
```
node.js version 14.16.1
```


## Installation benötigter Dependencies
```
npm install vis-network
npm install vis-data

npm install chart.js

npm install vue-loading-spinner
```


## Frontend ausführen
```
npm run build
serve -s dist
```
http://localhost:3000 im Browser aufrufen


## Anmerkungen
Die URL des Backends kann im File App.vue in Zeile 93 angepasst werden.

### Möglicherweise benötigt, um das Frontend starten zu können
```
npm run server
npm install -g serve
```
in windows power shell as admin:
```
set-executionpolicy unrestricted
```